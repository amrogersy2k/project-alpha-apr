from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import task_form
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = task_form(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = task_form()
        context = {"form": form}
        return render(request, "tasks/create_task.html", context)


@login_required
def list_tasks(request):
    list_tasks = Task.objects.filter(assignee=request.user)
    context = {"list_tasks": list_tasks}
    return render(request, "tasks/my_tasks.html", context)
