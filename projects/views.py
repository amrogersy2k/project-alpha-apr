from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import project_form
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = project_form(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = project_form()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
