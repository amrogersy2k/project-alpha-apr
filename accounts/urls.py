from django.urls import path
from accounts.views import show_login, show_signup, show_logout

urlpatterns = [
    path("login/", show_login, name="login"),
    path("signup/", show_signup, name="signup"),
    path("logout/", show_logout, name="logout"),
]
